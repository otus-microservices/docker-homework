1. Build:
docker build -t hound672/otus-health-server:0.0.1 .

2. Run:
docker run --rm -p 8000:8000 hound672/otus-health-server:0.0.1

3. Request:

curl localhost:8000/health

