package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

const (
	httpPort = 8000
)

type HealthResponse struct {
	Status string `json:"status"`
}

func health(w http.ResponseWriter, req *http.Request) {
	healthStatus := &HealthResponse{Status: "OK"}

	data, _ := json.Marshal(healthStatus)
	_, _ = w.Write(data)
	w.WriteHeader(200)
}

func main() {
	log.Printf("Start app")

	http.HandleFunc("/health", health)
	err := http.ListenAndServe(fmt.Sprintf(":%d", httpPort), nil)
	if err != nil {
		log.Fatalf("err start server: %v", err)
	}
}
