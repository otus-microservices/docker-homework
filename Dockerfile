FROM golang:1.18-alpine AS builder

WORKDIR /app

COPY main.go ./

RUN go build -v -o health_server main.go

FROM alpine:3.14

WORKDIR /app

COPY --from=builder /app/health_server ./

CMD ["/app/health_server"]
